import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'homePage',
    component: () => import('../views/homePage.vue')
  },
  {
    path: '/timePage',
    name: 'timePage',
    component: () => import('../views/timePage.vue')
  },
  // count页面
  {
    path: '/countPage',
    name: 'countPage',
    component: () => import('../views/countPage.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
